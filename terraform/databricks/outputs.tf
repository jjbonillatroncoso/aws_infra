output "databricks_cluster_id" {
  value     =  module.default_workspace.databricks_cluster_id
}

output "jdbc_url" {
  value = module.default_workspace.jdbc_url
}

output "odbc_params" {
  value = module.default_workspace.odbc_params
}

output "data_source_id" {
  value = module.default_workspace.data_source_id
}