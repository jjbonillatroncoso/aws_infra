terraform {
  required_providers {
    databricks = {
      source = "databricks/databricks"
      #configuration_aliases = [ databricks.default ]
   }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.15.0"
    }
  }
}

module "default_workspace" {
      source  = "../modules/databricks_workspace"
      # providers = {
      #       databricks = databricks.mws
      # }
      databricks_account_username = var.databricks_account_username
      databricks_account_password = var.databricks_account_password
      databricks_account_id = var.databricks_account_id
      prefix = var.prefix
      tags = var.tags
      region = var.region
      subnet_ids = var.subnet_ids
      aws_vpc =  var.aws_vpc
      default_security_group_id = var.default_security_group_id
      root_storage_bucket_id = var.root_storage_bucket_id

}


