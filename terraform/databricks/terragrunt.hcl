include "root" {
  path = find_in_parent_folders()
}

dependency "vpc" {
  config_path="../vpc"
} 

dependency "s3" {
  config_path="../s3"
}

locals {
  common = read_terragrunt_config(find_in_parent_folders("common.hcl"))
}

inputs = {
  tags = merge(local.common.locals,)
  subnet_ids = dependency.vpc.outputs.aws_subnet_private_subnets_ids
  aws_vpc = dependency.vpc.outputs.aws_vpc
  default_security_group_id = dependency.vpc.outputs.default_security_group_id
  root_storage_bucket_id = dependency.s3.outputs.root_storage_bucket_id
}