variable "databricks_account_username" {}
variable "databricks_account_password" {}
variable "databricks_account_id" {}

variable "prefix" {}
variable "tags" {type=map(string)}
variable "region" {}

variable subnet_ids {type=list(string)}
variable aws_vpc {}
variable default_security_group_id {}
variable root_storage_bucket_id {}
