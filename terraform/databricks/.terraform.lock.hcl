# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/databricks/databricks" {
  version = "1.2.1"
  hashes = [
    "h1:w3okiTzA4xzm63PPXHd8P1zp5B2/zKHHWzsxUcuwOBo=",
    "zh:200572265f00479461f27231f2e66b705ecc6039c3153a61bb6fc6e1fb4723a9",
    "zh:5ff7829fba65b7337f0d2bf9d461b6b03c0bae741a7a4bd276715b0e1de39bb1",
    "zh:672eadc69174a4a72cb5f2b8e6c28912467dc1dca44eb7c9ff312e53a5ca8cb7",
    "zh:ac0f2d6d12e43a126cc86e2f500c6ce35fe77c47e0b496c9aa2013c6dd15b12a",
    "zh:afcc187fc1c1b0576fb846c8f247df7ba322eb90f975406cf8ddf009002090bf",
    "zh:b2ae942f04fac2d89244a348a13fd7b071e7408b00fe3f035e017bf19de92fa5",
    "zh:bafb1759cbaa5dd385f101e533ee06c75621fece9f6e2fd2608bc8b0f5066723",
    "zh:e2dae7226e0e7761c9bfa6c7bd75f6f56f0fe83e290ce9e34719bb2ae85419d2",
    "zh:ec5c5769af7f552dbd4435a1954c44185212a4511b211bd250b472b22af0d965",
    "zh:fb5ddcaa4434f542eac8a6ebd165c78fa79b3c7e62cad9f0a90fb884a3c2f99b",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "4.15.1"
  constraints = "~> 4.15.0"
  hashes = [
    "h1:KNkM4pOCRzbjlGoCxt4Yl4qGUESLQ2uKIOSHb+aiMlY=",
    "zh:1d944144f8d613b8090c0c8391e4b205ca036086d70aceb4cdf664856fa8410c",
    "zh:2a0ca16a6b12c0ac509f64512f80bd2ed6e7ea0ec369212efd4be3fa65e9773d",
    "zh:3f9efdce4f1c320ffd061e8715e1d031deac1be0b959eaa60c25a274925653e4",
    "zh:4cf82f3267b0c3e08be29b0345f711ab84ea1ea75f0e8ce81f5a2fe635ba67b4",
    "zh:58474a0b7da438e1bcd53e87f10e28830836ff9b46cce5f09413c90952ae4f78",
    "zh:6eb1be8afb0314b6b8424fe212b13beeb04f3f24692f0f3ee86c5153c7eb2e63",
    "zh:8022da7d3b050d452ce6c679844e13729bdb4e1b3e75dcf68931af17a06b9277",
    "zh:8e2683d00fff1df43440d6e7c04a2c1eb432c7d5dacff32fe8ce9045bc948fe6",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:b0c22d9a306e8ac2de57b5291a3d0a7a2c1713e33b7d076005662451afdc4d29",
    "zh:ba6b7d7d91388b636145b133da6b4e32620cdc8046352e2dc8f3f0f81ff5d2e2",
    "zh:d38a816eb60f4419d99303136a3bb61a0d2df3ca8a1dce2ced9b99bf23efa9f7",
  ]
}
