variable s3_datalake_id {}

variable "prefix" {
  type        = string
  description = "A prefix to use when naming resources."
}

variable "tags" {
  type        = map(string)
  description = "common tags"
}
