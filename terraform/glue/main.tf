resource "aws_glue_catalog_database" "demo_database" {
  name = "demo-database"
}

resource "aws_iam_role" "crawler_role" {
  # Using a prefix ensures a unique name
  name_prefix = "${var.prefix}-crawler-role"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "glue.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })

  tags = var.tags
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole",
    "arn:aws:iam::aws:policy/AmazonS3FullAccess"
    ]
}

resource "aws_glue_crawler" "demo_crawler" {
  database_name = split(":",aws_glue_catalog_database.demo_database.id)[1]
  name          = "demo_data_crawler"
  role          = aws_iam_role.crawler_role.arn

  s3_target {
    path = "s3://${var.s3_datalake_id}/retail_db"
  }
}

