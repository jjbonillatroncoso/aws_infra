resource "aws_db_instance" "airflow_db" {
  allocated_storage    = 10
  engine               = "postgres"
  engine_version       = "13"
  instance_class       = "db.t3.micro"
  db_name              = "airflow_db"
  username             = var.db_airflow_user
  password             = var.db_airflow_pass
  skip_final_snapshot  = true
  db_subnet_group_name = var.eks_private_subs_id
  publicly_accessible = true
  vpc_security_group_ids =  [aws_security_group.eks_rds_access[0].id]

}

resource "aws_db_instance" "airflow_db_open" {
  allocated_storage    = 10
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t3.micro"
  db_name              = "airflow_db_open"
  username             = var.db_airflow_user
  password             = var.db_airflow_pass
  skip_final_snapshot  = true
  db_subnet_group_name = var.eks_private_subs_id
  publicly_accessible = true
  vpc_security_group_ids =  [aws_security_group.eks_rds_access[1].id]

}

resource "aws_db_instance" "openmetadata_db" {
  allocated_storage    = 10
  engine               = "mysql"
  engine_version       = "8.0"
  allow_major_version_upgrade = true
  instance_class       = "db.t3.micro"
  db_name              = "open_metadata_db"
  username             = var.db_open_user
  password             = var.db_open_pass
  skip_final_snapshot  = true
  db_subnet_group_name = var.eks_private_subs_id
  publicly_accessible = true
  vpc_security_group_ids =  [aws_security_group.eks_rds_access[1].id]

}


resource "aws_security_group" "eks_rds_access" {
  # ... other configuration ...
  vpc_id = var.aws_vpc
  count = 2

  egress {
              cidr_blocks      = ["0.0.0.0/0"]
              from_port        = "0"
              protocol         = "-1"
              to_port          = "0"
              description = "terraform generated"
  }

  ingress {
     
              cidr_blocks      = ["10.192.20.0/24","10.192.21.0/24"]
              from_port        = var.rds_ports[count.index]
              protocol         = "tcp"
              to_port          = var.rds_ports[count.index]
              description = "terraform generated"
        
  }
}