include "root" {
  path = find_in_parent_folders()
}


dependency "vpc" {
  config_path="../vpc"
} 

locals {
  common = read_terragrunt_config(find_in_parent_folders("common.hcl"))
}

inputs = {
  tags = merge(local.common.locals,)
  eks_private_subs_id= dependency.vpc.outputs.eks_private_subs_id
  aws_vpc = dependency.vpc.outputs.aws_vpc
}