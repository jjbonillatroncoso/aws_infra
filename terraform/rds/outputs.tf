output "rds_db_name_open" {
  value =  aws_db_instance.openmetadata_db.db_name
}

output "rds_db_host_open" {
  value =  aws_db_instance.openmetadata_db.address
}


output "rds_db_name_air" {
  value =  aws_db_instance.airflow_db.db_name
}

output "rds_db_host_air" {
  value =  aws_db_instance.airflow_db.address
}


output "rds_db_name_air_open" {
  value =  aws_db_instance.airflow_db_open.db_name
}

output "rds_db_host_air_open" {
  value =  aws_db_instance.airflow_db_open.address
}