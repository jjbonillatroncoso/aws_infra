variable db_open_pass {
    type = string
    description = "password for openmetadata database"
}

variable db_open_user {
    type = string
    description = "password for openmetadata database"
}


variable db_airflow_pass {
    type = string
    description = "password for openmetadata database"
}

variable db_airflow_user {
    type = string
    description = "password for openmetadata database"
}

variable eks_private_subs_id {
    type = string 
    description = "vpc private subs"
}

variable aws_vpc {
    type  = string
}


variable rds_ports {
    type  = list(number)
}
