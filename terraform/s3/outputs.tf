output "logs_bucket" {
  value =  aws_s3_bucket.airflow_logs.id
}

output "root_storage_bucket_id" {
  value = aws_s3_bucket.root_storage_bucket.id
}

output "s3_datalake_id" {
  value = aws_s3_bucket.s3_datalake.id
}

output "emr_bootstrap_id" {
  value = aws_s3_bucket.emr_bootstrap.id
}
