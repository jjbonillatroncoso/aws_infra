variable logs_bucket {
    type = string
    description = "s3 bucket for logs"
}

variable "databricks_account_username" {}
variable "databricks_account_password" {}
variable "databricks_account_id" {}
variable "datalake_bucket" {}

variable "prefix" {
  type        = string
  description = "A prefix to use when naming resources."
}

variable "tags" {
  type        = map(string)
  description = "common tags"
}
