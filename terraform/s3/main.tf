module "s3_data_repo" {
  source = "git::https://github.com/itversity/retail_db_json.git"
}

resource "aws_s3_bucket" "airflow_logs" {
  bucket = var.logs_bucket
  force_destroy = true
}

resource "aws_s3_bucket" "s3_datalake" {
  bucket = var.datalake_bucket
  force_destroy = true
}

resource "aws_s3_bucket_object" "s3_data" {
  for_each = fileset(".terraform/modules/s3_data_repo/", "**")

  bucket = aws_s3_bucket.s3_datalake.id
  key    = "retail_db/${each.value}"
  source = ".terraform/modules/s3_data_repo/${each.value}"
  # etag makes the file update when it changes; see https://stackoverflow.com/questions/56107258/terraform-upload-file-to-s3-on-every-apply
  etag   = filemd5(".terraform/modules/s3_data_repo/${each.value}")
}


resource "aws_s3_bucket_object" "emr_jars" {
  for_each = fileset("files/jars/", "**")

  bucket = aws_s3_bucket.emr_bootstrap.id
  key    = "${each.value}"
  source = "files/jars/${each.value}"
  # etag makes the file update when it changes; see https://stackoverflow.com/questions/56107258/terraform-upload-file-to-s3-on-every-apply
  etag   = filemd5("files/jars/${each.value}")
}

resource "aws_s3_bucket_object" "emr_data" {
  for_each = fileset("files/data/", "**")

  bucket = aws_s3_bucket.s3_datalake.id
  key    = "delta-demo/raw/${each.value}"
  source = "files/data/${each.value}"
  # etag makes the file update when it changes; see https://stackoverflow.com/questions/56107258/terraform-upload-file-to-s3-on-every-apply
  etag   = filemd5("files/data/${each.value}")
}


resource "aws_s3_bucket_object" "emr_script" {

  bucket = aws_s3_bucket.emr_bootstrap.id
  key    = "install_deltacore.sh"
  content = "sudo aws s3 cp s3://${aws_s3_bucket.emr_bootstrap.id}/delta-core_2.12-0.8.0.jar /usr/lib/spark/jars/"
}





resource "aws_s3_bucket" "root_storage_bucket" {
  bucket = "${var.prefix}-rootbucket"
  acl    = "private"
  force_destroy = true
  tags = merge(var.tags, {
    Name = "${var.prefix}-rootbucket"
  })
}

resource "aws_s3_bucket" "emr_bootstrap" {
  bucket = "${var.prefix}-emr-bootstrap"
  acl    = "private"
  force_destroy = true
  tags = merge(var.tags, {
    Name = "${var.prefix}-emr-bootstrap"
  })
}



resource "aws_s3_bucket_server_side_encryption_configuration" "root_storage_bucket" {
  bucket = aws_s3_bucket.root_storage_bucket.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "root_storage_bucket" {
  bucket                  = aws_s3_bucket.root_storage_bucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
  depends_on              = [aws_s3_bucket.root_storage_bucket]
}

