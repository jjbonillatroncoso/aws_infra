data "aws_availability_zones" "available" {}


module "vpc" {
  source  = "git::https://github.com/terraform-aws-modules/terraform-aws-vpc.git?ref=v3.2.0"  #"terraform-aws-modules/vpc/aws"
  #version = "3.2.0"

  name = var.prefix
  cidr = var.vpc_cidr
  azs  = data.aws_availability_zones.available.names
  tags = var.tags

  enable_dns_hostnames = true
  enable_nat_gateway   = true
  single_nat_gateway   = true
  create_igw           = true

  public_subnets = [cidrsubnet(var.vpc_cidr, 3, 0)]
  private_subnets = [cidrsubnet(var.vpc_cidr, 3, 1),
  cidrsubnet(var.vpc_cidr, 3, 2)]

  manage_default_security_group = true
  default_security_group_name   = "${var.prefix}-sg"

  default_security_group_egress = [{
    cidr_blocks = "0.0.0.0/0"
  }]

  default_security_group_ingress = [{
    description = "Allow all internal TCP and UDP"
    self        = true
  }]
}

module "vpc_endpoints" {
  source  = "terraform-aws-modules/vpc/aws//modules/vpc-endpoints"
  version = "3.2.0"

  vpc_id             = module.vpc.vpc_id
  security_group_ids = [module.vpc.default_security_group_id]

  endpoints = {
    s3 = {
      service      = "s3"
      service_type = "Gateway"
      route_table_ids = flatten([
        module.vpc.private_route_table_ids,
      module.vpc.public_route_table_ids])
      tags = {
        Name = "${var.prefix}-s3-vpc-endpoint"
      }
    },
    sts = {
      service             = "sts"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      tags = {
        Name = "${var.prefix}-sts-vpc-endpoint"
      }
    },
    kinesis-streams = {
      service             = "kinesis-streams"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      tags = {
        Name = "${var.prefix}-kinesis-vpc-endpoint"
      }
    },
    sqs = {
      service             = "sqs"
      subnet_ids          = module.vpc.private_subnets
      tags = {
        Name = "${var.prefix}-sqs-vpc-endpoint"
      }
    }
  }

  tags = var.tags
}

resource "aws_ec2_client_vpn_endpoint" "vpn" {
  description = "Client VPN example"
  client_cidr_block = "10.20.0.0/22"
  split_tunnel = true
  server_certificate_arn = var.server_certificate_arn

  authentication_options {
    type = "certificate-authentication"
    root_certificate_chain_arn = var.client_certificate_arn 
  }

  connection_log_options {
    enabled = false
  }

  tags = var.tags

}


resource "aws_security_group" "vpn_access" {
  vpc_id = module.vpc.vpc_id
  name = "vpn-example-sg"

  ingress {
    from_port = 443
    protocol = "UDP"
    to_port = 443
    cidr_blocks = ["0.0.0.0/0"]
    description = "Incoming VPN connection"
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}

resource "aws_ec2_client_vpn_network_association" "vpn_subnets" {
  count = length(module.vpc.private_subnets)

  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn.id
  subnet_id = module.vpc.private_subnets[count.index]
  security_groups = [aws_security_group.vpn_access.id]

  lifecycle {
    // The issue why we are ignoring changes is that on every change
    // terraform screws up most of the vpn assosciations
    // see: https://github.com/hashicorp/terraform-provider-aws/issues/14717
    ignore_changes = [subnet_id]
  }
}

resource "aws_ec2_client_vpn_authorization_rule" "vpn_auth_rule" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn.id
  target_network_cidr = var.vpc_cidr
  authorize_all_groups = true
}