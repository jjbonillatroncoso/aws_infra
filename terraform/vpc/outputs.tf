output "aws_subnet_private_subnets_ids" {
   value = [module.vpc.private_subnets[0], module.vpc.private_subnets[1]]
}

output "aws_vpc" {
   value = module.vpc.vpc_id
}

# output "eks_private_subs_id" {
#    value = aws_db_subnet_group.eks_private_subs.id
# }

output "default_security_group_id" {
   value = module.vpc.default_security_group_id
}

