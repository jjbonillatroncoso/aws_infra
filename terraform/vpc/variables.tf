variable "region" {
  type        = string
  description = "AWS region where resources will be deployed."
  default     = "us-east-1"
}

variable "prefix" {
  type        = string
  description = "A prefix to use when naming resources."
}

variable "tags" {
  type        = map(string)
  description = "common tags"
}

variable "vpc_cidr" {
  type        = string
  description = "VPC CIDR block."
  default = "10.192.0.0/16"
}

variable "public_subnet_cidrs" {
  type        = list(string)
  description = "Public subnets' CIDR blocks."
  default = [
    "10.192.10.0/24",
    "10.192.11.0/24"
  ]
}

variable "private_subnet_cidrs" {
  type        = list(string)
  description = "Private subnets' CIDR blocks."
  default = [
        "10.192.20.0/24",
        "10.192.21.0/24"
  ]
}

variable "cluster_id" {
  type = string
  description = "cluster id associated to cluster arn"
}

variable server_certificate_arn {}

variable client_certificate_arn {}