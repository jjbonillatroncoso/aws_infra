variable "region" {
  type        = string
  description = "AWS region where resources will be deployed."
  default     = "us-east-1"
}

variable "prefix" {
  type        = string
  description = "A prefix to use when naming resources."
}

variable "cluster_id" {
  type = string
  description = "cluster id associated to cluster arn"
}

variable "tags" {
  type        = map(string)
  description = "common tags"
}

variable "service_account_role_arn"{
  type = string
  description = "service account role arn for data services"
}

variable "service_account_name_data" {
  type        = string
  description = "subnet_ids"
}

variable "airflow_repo" {
  type = string 
  description = "helm release airflow image"
}

variable "acm_arn"{
  type        = string
  description = "acm certificate"
}

variable "staging_host"{
  type        = string
  description = "acm certificate"
}

variable "airflow_git_repo" {
  type = string 
  description = "airflow git repo"
}

variable "airflow_git_branch" {
  type = string 
  description = ""
}

variable "airflow_git_dest" {
  type = string 
  description = ""
}

variable "airflow_git_subpath" {
  type = string 
  description = ""
}


variable db_airflow_pass {
    type = string
    description = "password for openmetadata database"
}

variable db_airflow_user {
    type = string
    description = "password for openmetadata database"
}

variable logs_bucket {
    type = string
    description = "s3 bucket for logs"
}

variable db_airflow_host {
  type = string 
  description = "airflow db host"
}

variable db_airflow_name {
  type = string 
  description = "airflow db name"
}
