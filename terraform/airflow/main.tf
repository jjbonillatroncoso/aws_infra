data "aws_eks_cluster" "eks" {
  name = var.cluster_id
}

data "aws_eks_cluster_auth" "eks" {
  name = var.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

resource "kubernetes_namespace" "airflow_namespace" {
  metadata {
    annotations = {
      name = "example-annotation"
    }

    labels = {
      mylabel = "staging-namespace"
    }

    name = "airflow-staging"
  }
}


resource "kubernetes_role" "main_role" {
  metadata {
    name = "main-role"
    namespace = kubernetes_namespace.airflow_namespace.metadata.0.name
    labels = {
      test = "MyRole"
    }
  }

  rule {
    api_groups     = ["","apps","batch","extensions"]
    resources      = ["jobs"
      ,"pods"
      ,"pods/attach"
      ,"pods/exec"
      ,"pods/log"
      ,"pods/portforward"
      ,"secrets"
      ,"services"]
    
    verbs          = [
       "create"
      ,"delete"
      ,"describe"
      ,"get"
      ,"list"
      ,"patch"
      ,"update"
        ]
  }
}

resource "kubernetes_role_binding" "example" {
  metadata {
    name      = "main-role-binding"
    namespace = kubernetes_namespace.airflow_namespace.metadata.0.name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.main_role.metadata.0.name
  }
  subject {
    kind      = "User"
    name      = "airflow-service"
    api_group = "rbac.authorization.k8s.io"
  }

  subject {
    kind      = "User"
    name      = "main-user"
    api_group = "rbac.authorization.k8s.io"
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.airflow_service_account.metadata.0.name
    #api_group = "rbac.authorization.k8s.io"
  }

}



resource "kubernetes_service_account" "airflow_service_account" {
  metadata {
    name = var.service_account_name_data
    namespace = kubernetes_namespace.airflow_namespace.metadata.0.name
    annotations = {
      "eks.amazonaws.com/role-arn": var.service_account_role_arn
    }
  }
}

resource "kubernetes_ingress_v1" "example" {
  metadata {
    name = "airflow"
    namespace = kubernetes_namespace.airflow_namespace.metadata.0.name
    annotations = {
      "kubernetes.io/ingress.class" : "alb"
      "alb.ingress.kubernetes.io/scheme" : "internet-facing"
      #"alb.ingress.kubernetes.io/certificate-arn": var.acm_arn
      #"alb.ingress.kubernetes.io/ssl-policy": "ELBSecurityPolicy-TLS-1-1-2017-01"
    }
  }
  spec {
    default_backend {
      service {
        name = "airflow-webserver"
        port {
          number = 8080
        } 
      }
    }
    rule {
      #host = var.staging_host
      http {
        path {
          backend {
            service {
              name = "airflow-webserver"
              port {
                number = 8080
              }
            }
          }
          path = "/"
        }
      }
    }
  }
}