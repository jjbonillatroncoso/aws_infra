include "root" {
  path = find_in_parent_folders()
}


dependency "eks" {
  config_path="../eks"
} 

dependency "s3" {
  config_path="../s3"
}

dependency "rds" {
  config_path="../rds"
} 

locals {
  common = read_terragrunt_config(find_in_parent_folders("common.hcl"))
}

inputs = {
  tags = merge(local.common.locals,)
  cluster_id = dependency.eks.outputs.cluster_id
  service_account_role_arn = dependency.eks.outputs.service_account_role_arn
  logs_bucket = dependency.s3.outputs.logs_bucket
  db_airflow_host = dependency.rds.outputs.rds_db_host_air
  db_airflow_name = dependency.rds.outputs.rds_db_name_air

}