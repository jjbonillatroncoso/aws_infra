provider "helm" {
        kubernetes {
          host                   = data.aws_eks_cluster.eks.endpoint
          cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
          exec {
            api_version = "client.authentication.k8s.io/v1beta1"
            args        = ["eks", "get-token", "--cluster-name", var.cluster_id]
            command     = "aws"
          }
        }
    }


resource "helm_release" "airflow" {
  name       = "airflow"
  repository = "https://airflow.apache.org"
  chart      = "airflow"
  version    = "1.6.0"
  namespace  = kubernetes_namespace.airflow_namespace.metadata.0.name
  wait       = false

  values = [
    templatefile("values.yaml",{
      airflow_repo = "${var.airflow_repo}",
      airflow_git_repo = "${var.airflow_git_repo}"
      airflow_git_branch = "${var.airflow_git_branch}"
      airflow_git_dest = "${var.airflow_git_dest}"
      airflow_git_subpath = "${var.airflow_git_subpath}"
      service_account_role_arn = "${var.service_account_role_arn}"
      logs_bucket = "${var.logs_bucket}"
      db_airflow_user = "${var.db_airflow_user}"
      db_airflow_pass = "${var.db_airflow_pass}"
      db_airflow_host = "${var.db_airflow_host}"
      db_airflow_name = "${var.db_airflow_name}"
      })
  ]


}

