provider "helm" {
        kubernetes {
          host                   = data.aws_eks_cluster.eks.endpoint
          cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
          exec {
            api_version = "client.authentication.k8s.io/v1beta1"
            args        = ["eks", "get-token", "--cluster-name", var.cluster_id]
            command     = "aws"
          }
        }
    }


resource "helm_release" "openmetadata-dependencies" {
  name       = "openmetadata-dependencies"
  repository = "https://helm.open-metadata.org/"
  chart      = "openmetadata-dependencies"
  #version    = "1.6.0"
  namespace  = kubernetes_namespace.open_metadata_namespace.metadata.0.name
  wait       = false

  values = [
    templatefile("values_deps.yml",{
      airflow-mysql-secrets = kubernetes_secret.airflow-mysql-secrets.metadata.0.name
      storage_class = var.eks_nfs_storage_class
      dags_pvc = kubernetes_persistent_volume_claim.pvc_dags.metadata.0.name
      logs_pvc = kubernetes_persistent_volume_claim.pvc_logs.metadata.0.name
      db_open_host = var.db_airflow_host 
      db_airflow_user = var.db_airflow_user
      db_airflow_name = var.db_airflow_name
      db_airflow_pass = var.db_airflow_pass
      })
  ]


}


resource "helm_release" "openmetadata" {
  name       = "openmetadata"
  repository = "https://helm.open-metadata.org/"
  chart      = "openmetadata"
  #version    = "1.6.0"
  namespace  = kubernetes_namespace.open_metadata_namespace.metadata.0.name
  wait       = false

  values = [
    templatefile("values.yml",{
      airflow-secrets =  kubernetes_secret.airflow-secrets.metadata.0.name
      mysql-secrets =  kubernetes_secret.mysql_secrets.metadata.0.name
      db_open_host = var.db_open_host
      db_open_name = var.db_open_name
      db_open_user = var.db_open_user
      })
  ]

  depends_on = [
    helm_release.openmetadata-dependencies
  ]


}


