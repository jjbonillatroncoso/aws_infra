variable "region" {
  type        = string
  description = "AWS region where resources will be deployed."
  default     = "us-east-1"
}

variable "prefix" {
  type        = string
  description = "A prefix to use when naming resources."
}

variable "cluster_id" {
  type = string
  description = "cluster id associated to cluster arn"
}

variable "tags" {
  type        = map(string)
  description = "common tags"
}

variable "subnet_ids" {
  type        = list(string)
  description = "subnet_ids"
}

variable "aws_vpc" {
  type = string
  description = "eks cluster vpc"
}

variable eks_nfs_storage_class {
  type = string 
  description = "eks_nfs_storage_class"
}


variable db_open_pass {
    type = string
    description = "password for openmetadata database"
}

variable db_open_user {
    type = string
    description = "password for openmetadata database"
}



variable db_airflow_pass {
    type = string
    description = "password for openmetadata database"
}

variable db_airflow_user {
    type = string
    description = "password for openmetadata database"
}

variable db_airflow_host {
  type = string
}

variable db_open_host {
  type = string
}

variable db_open_name {
  type = string
}

variable db_airflow_name {
  type = string
}