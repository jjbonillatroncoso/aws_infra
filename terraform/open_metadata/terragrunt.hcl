include "root" {
  path = find_in_parent_folders()
}

# generate "provider" {
#     path = "helm.tf"
#     if_exists = "overwrite_terragrunt"
#     contents = <<EOF
#     provider "helm" {
#         kubernetes {
#           host                   = data.aws_eks_cluster.eks.endpoint
#           cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
#           exec {
#             api_version = "client.authentication.k8s.io/v1alpha1"
#             args        = ["eks", "get-token", "--cluster-name", var.cluster_id]
#             command     = "aws"
#           }
#         }
#     }
#     EOF
# }


dependency "eks" {
  config_path="../eks"
} 

dependency "vpc" {
  config_path="../vpc"
} 

dependency "rds" {
  config_path="../rds"
} 

locals {
  common = read_terragrunt_config(find_in_parent_folders("common.hcl"))
}

inputs = {
  tags = merge(local.common.locals,)
  cluster_id = dependency.eks.outputs.cluster_id
  service_account_role_arn = dependency.eks.outputs.service_account_role_arn,
  subnet_ids = dependency.vpc.outputs.aws_subnet_private_subnets_ids
  aws_vpc = dependency.vpc.outputs.aws_vpc
  eks_nfs_storage_class = dependency.eks.outputs.eks_nfs_storage_class
  #efs_dns = dependency.eks.outputs.efs_dns
  db_airflow_host = dependency.rds.outputs.rds_db_host_air_open
  db_airflow_name = dependency.rds.outputs.rds_db_name_air_open
  db_open_host = dependency.rds.outputs.rds_db_host_open
  db_open_name = dependency.rds.outputs.rds_db_name_open
}