data "aws_eks_cluster" "eks" {
  name = var.cluster_id
}

data "aws_eks_cluster_auth" "eks" {
  name = var.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

resource "kubernetes_namespace" "open_metadata_namespace" {
  metadata {
    labels = {
      mylabel = "open-metadata"
    }
    name = "open-metadata"
  }
}

resource "kubernetes_secret" "mysql_secrets" {
  metadata {
    name = "mysql-secrets"
    namespace = kubernetes_namespace.open_metadata_namespace.metadata.0.name
  }

  type = "generic"

  
  data = {
    openmetadata-mysql-password=var.db_open_pass #"openmetadata_password"
  }
  
}

resource "kubernetes_secret" "airflow-secrets" {
  metadata {
    name = "airflow-secrets"
    namespace = kubernetes_namespace.open_metadata_namespace.metadata.0.name
  }

  type = "generic"

  
  data = {
    openmetadata-airflow-password=var.db_airflow_pass #"airflow_pass"
  }
  
}

resource "kubernetes_secret" "airflow-mysql-secrets" {
  metadata {
    name = "airflow-mysql-secrets"
    namespace = kubernetes_namespace.open_metadata_namespace.metadata.0.name
  }

  type = "generic"

  
  data = {
    airflow-mysql-password=var.db_airflow_pass
  }
  
}


resource "kubernetes_persistent_volume_claim" "pvc_dags" {
  metadata {
    name = "pvc-dags"
    namespace = kubernetes_namespace.open_metadata_namespace.metadata.0.name
  }
  spec {
    access_modes = ["ReadWriteMany"]
    storage_class_name = var.eks_nfs_storage_class
    resources {  
      requests = {
        storage = "10Gi"
      }
    } 
  }
  
 
}



resource "kubernetes_persistent_volume_claim" "pvc_logs" {
  metadata {
    name = "pvc-logs"
    namespace = kubernetes_namespace.open_metadata_namespace.metadata.0.name
  }
  spec {
    access_modes = ["ReadWriteMany"]
    storage_class_name = var.eks_nfs_storage_class
    resources {  
      requests = {
        storage = "10Gi"
      }
    } 
  }
  
 
}

resource "kubernetes_ingress_v1" "open_metadata" {
  metadata {
    name = "openmetadata"
    namespace = kubernetes_namespace.open_metadata_namespace.metadata.0.name
    annotations = {
      "kubernetes.io/ingress.class" : "alb"
      "alb.ingress.kubernetes.io/scheme" : "internet-facing"
      "alb.ingress.kubernetes.io/target-type": "ip"
      #"alb.ingress.kubernetes.io/certificate-arn": var.acm_arn
      #"alb.ingress.kubernetes.io/ssl-policy": "ELBSecurityPolicy-TLS-1-1-2017-01"
    }
  }
  spec {
    default_backend {
      service {
        name = "openmetadata"
        port {
          number = 8585
        } 
      }
    }
    rule {
      #host = var.staging_host
      http {
        path {
          backend {
            service {
              name = "openmetadata"
              port {
                number = 8585
              }
            }
          }
          path = "/"
        }
      }
    }
  }
} 
