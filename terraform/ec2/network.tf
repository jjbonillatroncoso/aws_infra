resource "aws_network_interface" "test" {
  subnet_id       = var.subnet_ids[0]
  security_groups = [aws_security_group.ec2_sqs_demo.id]
  count = var.ec2_count

  tags = {
    Name = "ec2-network-interface-${count.index}"
  }

}

resource "aws_security_group" "ec2_sqs_demo" {
  # ... other configuration ...
  name = "ec2-sqs-demo"
  vpc_id = var.aws_vpc

  egress {
              cidr_blocks      = ["0.0.0.0/0"]
              from_port        = "0"
              protocol         = "-1"
              to_port          = "0"
              description = "terraform generated"
  }

  ingress {
     
              cidr_blocks      = ["0.0.0.0/0"]
              from_port        = 22
              protocol         = "tcp"
              to_port          = 22
              description = "terraform generated"
        
  }
}