data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "ec2_sqs_demo" {
  ami                   = data.aws_ami.ubuntu.id
  instance_type         = "t2.micro"
  key_name              = var.ec2_key_name
  count                 = var.ec2_count
  iam_instance_profile  = aws_iam_instance_profile.ec2-sqs-demo.id


  network_interface {
    network_interface_id = aws_network_interface.test[count.index].id
    device_index = 0
  }

  tags = {
    Name = "ec2-sqs-demo-${count.index}"
  }

}