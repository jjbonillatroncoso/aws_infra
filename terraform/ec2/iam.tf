resource "aws_iam_policy" "ec2-sqs-demo" {
    policy = file("policies/ec2-sqs-demo.json")
    name = "ec2-sqs-demo-policy"
}

resource "aws_iam_role" "ec2-sqs-demo" {
    name = "ec2-sqs-demo-role"
    assume_role_policy = <<POLICY
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid":"",
                "Effect":"Allow",
                "Principal": {
                    "Service": [
                        "ec2.amazonaws.com"
                    ]
                },
                "Action": "sts:AssumeRole"
            }
        ]
    }
    POLICY
}

resource "aws_iam_instance_profile" "ec2-sqs-demo" {
    name = "ec2-sqs-demo"
    role = aws_iam_role.ec2-sqs-demo.name
}

resource "aws_iam_role_policy_attachment" "ec2-sqs-demo" {
    role = aws_iam_role.ec2-sqs-demo.name
    policy_arn = aws_iam_policy.ec2-sqs-demo.arn 
}

