include "root" {
  path = find_in_parent_folders()
}

locals {
  common = read_terragrunt_config(find_in_parent_folders("common.hcl"))
}

dependency "vpc" {
  config_path="../vpc"
}


inputs = {
  tags = merge(local.common.locals,)
  subnet_ids = dependency.vpc.outputs.aws_subnet_private_subnets_ids
  aws_vpc = dependency.vpc.outputs.aws_vpc
}