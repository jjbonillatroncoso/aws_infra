variable "subnet_ids" {
  type        = list(string)
  description = "subnet_ids"
}

variable "aws_vpc" {
  type = string
  description = "eks cluster vpc"
}

variable "ec2_count" {
    type=number
    default = 1
}

variable ec2_key_name {}