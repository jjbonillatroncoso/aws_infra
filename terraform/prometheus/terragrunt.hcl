include "root" {
  path = find_in_parent_folders()
}


dependency "eks" {
  config_path="../eks"
} 


locals {
  common = read_terragrunt_config(find_in_parent_folders("common.hcl"))
}

inputs = {
  tags = merge(local.common.locals,)
  cluster_id = dependency.eks.outputs.cluster_id

}