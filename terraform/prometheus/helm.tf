provider "helm" {
        kubernetes {
          host                   = data.aws_eks_cluster.eks.endpoint
          cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
          exec {
            api_version = "client.authentication.k8s.io/v1beta1"
            args        = ["eks", "get-token", "--cluster-name", var.cluster_id]
            command     = "aws"
          }
        }
    }


resource "helm_release" "prometheus" {
  name       = "prometheus"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  namespace  = kubernetes_namespace.monitoring.metadata.0.name
  wait       = false

  values = [
    /*template*/ file("values.yml")
  ]


}
