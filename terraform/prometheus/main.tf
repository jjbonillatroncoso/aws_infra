data "aws_eks_cluster" "eks" {
  name = var.cluster_id
}

data "aws_eks_cluster_auth" "eks" {
  name = var.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}


resource "kubernetes_ingress_v1" "prometheus" {
  metadata {
    name = "prometheus"
    namespace = kubernetes_namespace.monitoring.metadata.0.name
    annotations = {
      "kubernetes.io/ingress.class" : "alb"
      "alb.ingress.kubernetes.io/scheme" : "internet-facing"
      "alb.ingress.kubernetes.io/target-type" : "ip"
      #"alb.ingress.kubernetes.io/certificate-arn": var.acm_arn
      #"alb.ingress.kubernetes.io/ssl-policy": "ELBSecurityPolicy-TLS-1-1-2017-01"
    }
  }
  spec {
    default_backend {
      service {
        name = "prometheus-kube-prometheus-prometheus"
        port {
          number = 9090
        } 
      }
    }
    rule {
      #host = var.staging_host
      http {
        path {
          backend {
            service {
              name = "prometheus-kube-prometheus-prometheus"
              port {
                number = 9090
              }
            }
          }
          path = "/"
        }
      }
    }
  }
}


resource "kubernetes_ingress_v1" "alert_manager" {
  metadata {
    name = "alert-manager"
    namespace = kubernetes_namespace.monitoring.metadata.0.name
    annotations = {
      "kubernetes.io/ingress.class" : "alb"
      "alb.ingress.kubernetes.io/scheme" : "internet-facing"
      "alb.ingress.kubernetes.io/target-type" : "ip"
      #"alb.ingress.kubernetes.io/certificate-arn": var.acm_arn
      #"alb.ingress.kubernetes.io/ssl-policy": "ELBSecurityPolicy-TLS-1-1-2017-01"
    }
  }
  spec {
    default_backend {
      service {
        name = "prometheus-kube-prometheus-alertmanager"
        port {
          number = 9093
        } 
      }
    }
    rule {
      #host = var.staging_host
      http {
        path {
          backend {
            service {
              name = "prometheus-kube-prometheus-alertmanager"
              port {
                number = 9093
              }
            }
          }
          path = "/"
        }
      }
    }
  }
}


resource "kubernetes_ingress_v1" "grafana" {
  metadata {
    name = "grafana"
    namespace = kubernetes_namespace.monitoring.metadata.0.name
    annotations = {
      "kubernetes.io/ingress.class" : "alb"
      "alb.ingress.kubernetes.io/scheme" : "internet-facing"
      "alb.ingress.kubernetes.io/target-type" : "ip"
      #"alb.ingress.kubernetes.io/certificate-arn": var.acm_arn
      #"alb.ingress.kubernetes.io/ssl-policy": "ELBSecurityPolicy-TLS-1-1-2017-01"
    }
  }
  spec {
    default_backend {
      service {
        name = "prometheus-grafana"
        port {
          name = "http-web"
        } 
      }
    }
    rule {
      #host = var.staging_host
      http {
        path {
          backend {
            service {
              name = "prometheus-grafana"
              port {
                name = "http-web"
              }
            }
          }
          path = "/"
        }
      }
    }
  }
}