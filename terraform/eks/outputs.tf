output "cluster_id" {
  value = aws_eks_cluster.cluster.id
}

output "service_account_role_arn" {
    value = aws_iam_role.service_account_role.arn
}

output "eks_nfs_storage_class" {
    value = kubernetes_storage_class.efs.metadata.0.name
}