resource "aws_iam_policy" "alb_policy" {
  name        = "alb_policy"
  path        = "/"
  description = "Permissions for alb"

  
  policy = file("templates/iam_policy.json")  

}

resource "aws_iam_policy" "data_policy" {
  name        = "data_policy"
  path        = "/"
  description = "Permissions for data services"

  
  policy = file("templates/iam_data_policy.json")  

}

resource "aws_iam_policy" "efs_policy" {
  name        = "eks_efs_policy"
  path        = "/"
  description = "Permissions for data services"

  
  policy = file("templates/iam_policy_efs.json")  

}