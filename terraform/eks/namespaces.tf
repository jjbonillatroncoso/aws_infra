resource "kubernetes_namespace" "storage_namespace" {
  metadata {
    annotations = {
      name = "storage"
    }

    labels = {
      mylabel = "storage"
    }

    name = "storage"
  }
}

