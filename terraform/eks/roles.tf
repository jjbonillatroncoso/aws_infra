resource "aws_iam_role" "cluster" {
  # Using a prefix ensures a unique name
  name_prefix = "eks-cluster-${var.cluster_id}-"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "eks.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })

  tags = var.tags
}

resource "aws_iam_role" "nodes" {
  name_prefix = "eks-nodes-${var.cluster_id}-"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })

  tags = var.tags
}

resource "aws_iam_role" "service_account_role" {
  name = "service_account_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy =  templatefile("templates/oidc_assume_role_policy.json", { OIDC_ARN = aws_iam_openid_connect_provider.cluster.arn, OIDC_URL = replace(aws_iam_openid_connect_provider.cluster.url, "https://", ""), NAMESPACE = "*", SA_NAME = "*" })

  tags = {
    tag-key = "tag-value"
  }
}

resource "aws_iam_role" "alb_service_account_role" {
  name = "alb_service_account_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy =  templatefile("templates/alb_assume_role_policy.json", { OIDC_ARN = aws_iam_openid_connect_provider.cluster.arn, OIDC_URL = replace(aws_iam_openid_connect_provider.cluster.url, "https://", "")})

  tags = {
    tag-key = "tag-value"
  }
}

resource "aws_iam_role" "nfs_service_account_role" {
  name = "nfs_service_account_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy =  templatefile("templates/alb_assume_role_policy.json", { OIDC_ARN = aws_iam_openid_connect_provider.cluster.arn, OIDC_URL = replace(aws_iam_openid_connect_provider.cluster.url, "https://", "")})

  tags = {
    tag-key = "tag-value"
  }
}



