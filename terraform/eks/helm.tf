data "aws_eks_cluster" "eks" {
  name = aws_eks_cluster.cluster.name
}

data "aws_eks_cluster_auth" "eks" {
  name = aws_eks_cluster.cluster.name
}

data "tls_certificate" "eks" {
  url =  aws_eks_cluster.cluster.identity.0.oidc.0.issuer
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

provider "helm" {
        kubernetes {
          host                   = data.aws_eks_cluster.eks.endpoint
          cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
          exec {
            api_version = "client.authentication.k8s.io/v1beta1"
            args        = ["eks", "get-token", "--cluster-name", var.cluster_id]
            command     = "aws"
          }
        }
    }




resource "helm_release" "alb" {
  name       = "alb"
  repository = "https://aws.github.io/eks-charts"
  chart      = "aws-load-balancer-controller"
  version    = "1.4.2"  #2.4.2
  namespace  = "kube-system"
  wait       = false

  set {
    name  = "clusterName"
    value = aws_eks_cluster.cluster.name
  }

  set {
    name  = "serviceAccount.create"
    value = "false"
  }

  set {
    name  = "serviceAccount.name"
    value =  kubernetes_service_account.aws-load-balancer-controller.metadata.0.name
  }


}


