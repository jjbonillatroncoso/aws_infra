variable "region" {
  type        = string
  description = "AWS region where resources will be deployed."
  default     = "us-east-1"
}

variable "prefix" {
  type        = string
  description = "A prefix to use when naming resources."
}

variable "cluster_id" {
  type = string
  description = "cluster id associated to cluster arn"
}

variable "tags" {
  type        = map(string)
  description = "common tags"
}

variable "subnet_ids" {
  type        = list(string)
  description = "subnet_ids"
}

variable "service_account_name_data"{
  type        = string
  description = "subnet_ids"
}

variable "aws_vpc" {
  type = string
  description = "eks cluster vpc"
}

variable "open_nfs_dns" {
  type = string 
  description = "nfs dns"
}