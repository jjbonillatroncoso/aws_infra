resource "aws_iam_role_policy_attachment" "cluster_eks_cluster_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.cluster.name
}

resource "aws_iam_role_policy_attachment" "nodes_eks_worker_node_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.nodes.name
}

resource "aws_iam_role_policy_attachment" "nodes_eks_cni_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.nodes.name
}

resource "aws_iam_role_policy_attachment" "nodes_ec2_container_registry_read_only" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.nodes.name
}

resource "aws_iam_role_policy_attachment" "alb_policy_attach" {
  policy_arn = aws_iam_policy.alb_policy.arn
  role       = aws_iam_role.alb_service_account_role.id
}

resource "aws_iam_role_policy_attachment" "data_policy_attach" {
  policy_arn = aws_iam_policy.data_policy.arn
  role       =  aws_iam_role.service_account_role.id
}

resource "aws_iam_role_policy_attachment" "efs_policy_attach" {
  policy_arn = aws_iam_policy.efs_policy.arn
  role       = aws_iam_role.nfs_service_account_role.id
}