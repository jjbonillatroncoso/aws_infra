resource "kubernetes_service_account" "efs-sa" {
  metadata {
    name = "efs-sa"
    namespace = kubernetes_namespace.storage_namespace.metadata.0.name
    annotations = {
      "eks.amazonaws.com/role-arn": aws_iam_role.nfs_service_account_role.arn
    }
  }
}


resource "kubernetes_cluster_role" "nfs_client_provisioner_runner" {
  metadata {
    name = "nfs-client-provisioner-runner"
  }

  rule {
    api_groups = [""]
    resources  = ["persistentvolumes"]
    verbs      = ["get", "list", "watch","create","delete"]
  }
    rule {
    api_groups = [""]
    resources  = ["persistentvolumeclaims"]
    verbs      = ["get", "list", "watch","update"]
  }
    rule {
    api_groups = ["storage.k8s.io"]
    resources  = ["storageclasses"]
    verbs      = ["get", "list", "watch"]
  }
    rule {
    api_groups = [""]
    resources  = ["events"]
    verbs      = ["create", "update", "patch"]
  }
}

resource "kubernetes_cluster_role_binding" "nfs_client_provisioner" {
  metadata {
    name = "nfs-client-provisioner"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.nfs_client_provisioner_runner.metadata.0.name
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.efs-sa.metadata.0.name
    namespace = kubernetes_namespace.storage_namespace.metadata.0.name
  }

}

resource "kubernetes_role" "leader_locking_nfs_client_provisioner" {
  metadata {
    name = "leader-locking-nfs-client-provisioner"
    namespace = kubernetes_namespace.storage_namespace.metadata.0.name
  }

  rule {
    api_groups     = [""]
    resources      = ["endpoints"]
    verbs          = ["get", "list", "watch","create","update","patch"]
  }

}

resource "kubernetes_role_binding_v1" "leader_locking_nfs_client_provisioner" {
  metadata {
    name      = "leader-locking-nfs-client-provisioner"
    namespace = kubernetes_namespace.storage_namespace.metadata.0.name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.leader_locking_nfs_client_provisioner.metadata.0.name
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.efs-sa.metadata.0.name
    namespace = kubernetes_namespace.storage_namespace.metadata.0.name
  }
}

resource "kubernetes_storage_class" "efs" {
  metadata {
    name = "efs"
  }
  storage_provisioner = "k8s-sigs.io/nfs-subdir-external-provisioner"
  parameters = {
    achieve_on_delete = false
  }
}

resource "kubernetes_deployment_v1" "nfs_client_provisioner" {
  metadata {
    name = "nfs-client-provisioner"
    namespace = kubernetes_namespace.storage_namespace.metadata.0.name

  }

  spec {
    replicas = 1
    strategy {
      type = "Recreate"
    }

    selector {
      match_labels = {
        app = "nfs-client-provisioner"
      }
    }

    template {
      metadata {
        labels = {
          app = "nfs-client-provisioner"
        }
      }

      spec {
        service_account_name = kubernetes_service_account.efs-sa.metadata.0.name
        container {
          image = "k8s.gcr.io/sig-storage/nfs-subdir-external-provisioner:v4.0.2"
          name  = "nfs-client-provisioner"
          volume_mount {
            name = "nfs-client-root"
            mount_path = "/persistentvolumes"
          }
          env {
            name = "PROVISIONER_NAME"
            value = "k8s-sigs.io/nfs-subdir-external-provisioner"
          }

          env {
            name = "NFS_SERVER"
            value = var.open_nfs_dns
            #value = "10.192.20.154"
          }

          env {
            name = "NFS_PATH"
            value = "/"
          }

      
        }
        volume {
          name = "nfs-client-root"
          nfs {
            server = var.open_nfs_dns
            #server = "10.192.20.154"
            path = "/"
          }
          }
        }
      }
    }
}

