data "aws_caller_identity" "current" {}

# Call ml-pipeline module
# Pipeline to train and deploy endpoint using stepfunctions

module "ml-pipeline" {
  source = "../modules/sagemaker_pipeline"
  project_name = var.project_name
  training_instance_type = var.training_instance_type
  inference_instance_type = var.inference_instance_type
  volume_size_sagemaker = var.volume_size_sagemaker
  s3_bucket_input_training_path = var.s3_bucket_input_training_path  
  s3_bucket_output_models_path = var.s3_bucket_output_models_path
  lambda_function_name = var.lambda_function_name
  handler_path = var.handler_path
  handler = var.handler
  lambda_folder = var.lambda_folder
  lambda_zip_filename = var.lambda_zip_filename
  ml_ecr_image=var.ml_ecr_image
}

# Inference Endpoint using serialized model stored in s3 


resource "aws_sagemaker_model" "inference_model_demo" {
  name               = "inference-model-demo"
  execution_role_arn = aws_iam_role.ml_demo_sagemaker_exec_tf.arn


  primary_container {
    image = var.ml_ecr_image
    model_data_url     = var.serialed_model_object 
  }
}

resource "aws_sagemaker_endpoint_configuration" "ec" {
  name = "inference-model-demo-config"

  production_variants {
    variant_name           = "variant-1"
    model_name             = aws_sagemaker_model.inference_model_demo.name
    initial_instance_count = 1
    instance_type          = "ml.m5.large"
  }

  async_inference_config {
    output_config {
      s3_output_path = var.s3_bucket_output_models_path
      notification_config {
        error_topic = aws_sns_topic.sagemaker_updates.id
      }
    }
    client_config {
      max_concurrent_invocations_per_instance = 1
    }

  }

}


resource "aws_sagemaker_endpoint" "demo_endpoint" {
  name                 = "inference-model-demo-endpoint"
  endpoint_config_name = aws_sagemaker_endpoint_configuration.ec.name

}

resource "aws_appautoscaling_target" "sagemaker_target" {
  max_capacity       = 1
  min_capacity       = 0
  resource_id        = "endpoint/${aws_sagemaker_endpoint.demo_endpoint.name}/variant/variant-1"
  #role_arn           = aws_iam_role.ml_demo_sagemaker_exec_tf.arn
  scalable_dimension = "sagemaker:variant:DesiredInstanceCount"
  service_namespace  = "sagemaker"
}

resource "aws_appautoscaling_policy" "sagemaker_policy" {
  name               = "sagemaker-target-tracking"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.sagemaker_target.resource_id
  scalable_dimension = aws_appautoscaling_target.sagemaker_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.sagemaker_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "SageMakerVariantInvocationsPerInstantarce"
    }
    # customized_metric_specification {
    #   metric_name = "ApproximateBacklogSizePerInstance"
    #   statistic   = "Average"
    #   namespace   =  aws_appautoscaling_target.sagemaker_target.service_namespace
    # }
    target_value       = 5
    scale_in_cooldown  = 200
    scale_out_cooldown = 300
  }
}