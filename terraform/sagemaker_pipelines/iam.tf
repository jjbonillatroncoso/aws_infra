
resource "aws_iam_role" "ml_demo_sagemaker_exec_tf" {
    name = "ml-demo-sagemaker-exec-tf"
    assume_role_policy = <<POLICY
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid":"",
                "Effect":"Allow",
                "Principal": {
                    "Service": [
                        "sagemaker.amazonaws.com"
                    ]
                },
                "Action": "sts:AssumeRole"
            }
        ]
    }
    POLICY
}

resource "aws_iam_role_policy_attachment" "sagemaker_permissions" {
    role = aws_iam_role.ml_demo_sagemaker_exec_tf.name
    policy_arn = "arn:aws:iam::aws:policy/AmazonSageMakerFullAccess"
}

resource "aws_iam_role_policy_attachment" "sqs_permissions" {
    role = aws_iam_role.ml_demo_sagemaker_exec_tf.name
    policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

resource "aws_iam_role_policy_attachment" "s3_permissions" {
    role = aws_iam_role.ml_demo_sagemaker_exec_tf.name
    policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "sns_permissions" {
    role = aws_iam_role.ml_demo_sagemaker_exec_tf.name
    policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

