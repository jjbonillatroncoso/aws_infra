output "databricks_host" {
  value = databricks_mws_workspaces.this.workspace_url
}

output "databricks_token" {
  value     = databricks_mws_workspaces.this.token[0].token_value
  sensitive = true
}

output "databricks_cluster_id" {
  value     =  databricks_cluster.single_node.id
}

output "jdbc_url" {
  value = databricks_sql_endpoint.this.jdbc_url
}

output "odbc_params" {
  value = databricks_sql_endpoint.this.odbc_params
}

output "data_source_id" {
  value = databricks_sql_endpoint.this.data_source_id
}