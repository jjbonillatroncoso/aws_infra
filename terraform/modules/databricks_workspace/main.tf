
// initialize provider in "MWS" mode to provision new workspace

terraform {
  required_providers {
    databricks = {
      source = "databricks/databricks"
      #configuration_aliases = [ databricks.default ]
   }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.15.0"
    }
  }
}

provider "databricks" {
  alias = "default"
  host  = databricks_mws_workspaces.this.workspace_url
  token = databricks_mws_workspaces.this.token[0].token_value
}

provider "databricks" {
  alias = "mws"
  host     = "https://accounts.cloud.databricks.com"
  username = var.databricks_account_username
  password = var.databricks_account_password
}


data "databricks_aws_assume_role_policy" "this" {
  external_id = var.databricks_account_id
}



data "databricks_aws_crossaccount_policy" "this" {
}


resource "databricks_mws_credentials" "this" {
  provider         = databricks.mws
  account_id       = var.databricks_account_id
  role_arn         = aws_iam_role.cross_account_role.arn
  credentials_name = "${var.prefix}-creds"
  depends_on       = [aws_iam_role_policy.this]
}

resource "databricks_mws_networks" "this" {
  provider           = databricks.mws
  account_id         = var.databricks_account_id
  network_name       = "${var.prefix}-network"
  security_group_ids = [var.default_security_group_id]
  subnet_ids         = var.subnet_ids
  vpc_id             = var.aws_vpc
}

# data "databricks_aws_bucket_policy" "this" {
#   bucket = aws_s3_bucket.root_storage_bucket.bucket
# }

data "databricks_aws_bucket_policy" "this" {
  bucket = var.root_storage_bucket_id 
}

# resource "databricks_mws_storage_configurations" "this" {
#   provider                   = databricks.mws
#   account_id                 = var.databricks_account_id
#   bucket_name                = aws_s3_bucket.root_storage_bucket.bucket
#   storage_configuration_name = "${var.prefix}-storage"
# }

resource "databricks_mws_storage_configurations" "this" {
  provider                   = databricks.mws
  account_id                 = var.databricks_account_id
  bucket_name                = var.root_storage_bucket_id 
  storage_configuration_name = "${var.prefix}-storage"
}

resource "databricks_mws_workspaces" "this" {
  provider       = databricks.mws
  account_id     = var.databricks_account_id
  aws_region     = var.region
  workspace_name = var.prefix

  credentials_id           = databricks_mws_credentials.this.credentials_id
  storage_configuration_id = databricks_mws_storage_configurations.this.storage_configuration_id
  network_id               = databricks_mws_networks.this.network_id

  token {
    comment = "Terraform"
  }
}

data "databricks_node_type" "smallest" {
  local_disk = true
  provider = databricks.default
  depends_on = [
    databricks_mws_workspaces.this
  ]
}

data "databricks_spark_version" "latest_lts" {
  provider = databricks.default
  long_term_support = true
  depends_on = [
    databricks_mws_workspaces.this
  ]
}


resource "databricks_instance_profile" "shared" {
  instance_profile_arn = aws_iam_instance_profile.shared.arn
  provider = databricks.default
}

resource "databricks_cluster" "single_node" {
  provider = databricks.default
  cluster_name            = "Single Node"
  spark_version           = data.databricks_spark_version.latest_lts.id
  node_type_id            = data.databricks_node_type.smallest.id
  autotermination_minutes = 20

  spark_conf = {
    # Single-node
    "spark.databricks.cluster.profile" : "singleNode"
    "spark.master" : "local[*]"
    "spark.databricks.service.port": "8787"
    "spark.databricks.service.server.enabled": "true"
    "spark.databricks.hive.metastore.glueCatalog.enabled": "true"
  }

  custom_tags = {
    "ResourceClass" = "SingleNode"
  }

  library {
    jar = "s3://jj-wizeline-emr-bootstrap/json-udf-1.3.8-jar-with-dependencies.jar"
  }

  library {
    jar = "s3://jj-wizeline-emr-bootstrap/json-serde-1.3.8-jar-with-dependencies.jar"
  }

  # library {
  #   jar = "s3://jj-wizeline-emr-bootstrap/json-serde-cdh5-shim-1.3.7.3.jar"
  # }

  # library {
  #   jar = "s3://jj-wizeline-emr-bootstrap/hive-serde-1.2.1.spark2.jar"
  # }

   aws_attributes {
    instance_profile_arn   = databricks_instance_profile.shared.id
    zone_id = "us-east-1a"
  }
}


resource "databricks_sql_endpoint" "this" {
  name             = "Endpoint of dbt"
  provider = databricks.default
  cluster_size     = "Small"
  max_num_clusters = 1

  tags {
    custom_tags {
      key   = "City"
      value = "Amsterdam"
    }
  }
}