resource "aws_iam_role" "cross_account_role" {
  name               = "${var.prefix}-crossaccount"
  assume_role_policy = data.databricks_aws_assume_role_policy.this.json
  tags               = var.tags
}

resource "aws_iam_role_policy" "this" {
  name   = "${var.prefix}-policy"
  role   = aws_iam_role.cross_account_role.id
  policy = data.databricks_aws_crossaccount_policy.this.json
}


resource "aws_iam_role" "bricks_profile" {
  name               = "${var.prefix}-bricks-profile"
  assume_role_policy = file("templates/role_policy_profile.json")
  tags               = var.tags
  managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonS3FullAccess"]
}

resource "aws_iam_policy" "root_policy" {
  name        = "bricks_root_policy"
  path        = "/"
  policy = templatefile("templates/permission_policy.json",{ MAIN_ROLE = aws_iam_role.bricks_profile.arn}) 

}

resource "aws_iam_policy" "bricks_profile" {
  name        = "bricks_profile_policy"
  path        = "/"
  policy = file("templates/profile_policy.json") 

}

resource "aws_iam_role_policy_attachment" "bricks_root_policy_attach" {
  policy_arn = aws_iam_policy.root_policy.arn
  role       = aws_iam_role.cross_account_role.id
}


resource "aws_iam_role_policy_attachment" "bricks_profile_policy_attach" {
  policy_arn = aws_iam_policy.bricks_profile.arn
  role       = aws_iam_role.bricks_profile.id
}

resource "aws_iam_instance_profile" "shared" {
  name = "shared-instance-profile"
  role = aws_iam_role.bricks_profile.name
}
