variable "s3_datalake_id" {}
variable "key_name" {}
variable "emr_bootstrap_id" {}
variable "step_instruction" {
    default = "sudo /usr/lib/spark/sbin/start-thriftserver.sh --conf spark.sql.extensions=io.delta.sql.DeltaSparkSessionExtension --conf spark.sql.catalog.spark_catalog=org.apache.spark.sql.delta.catalog.DeltaCatalog --conf spark.delta.logStore.class=org.apache.spark.sql.delta.storage.S3SingleDriverLogStore"
}