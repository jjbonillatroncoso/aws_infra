include "root" {
  path = find_in_parent_folders()
}

locals {
  common = read_terragrunt_config(find_in_parent_folders("common.hcl"))
}

dependency "s3" {
  config_path="../s3"
}


inputs = {
  tags = merge(local.common.locals,)
  s3_datalake_id = dependency.s3.outputs.s3_datalake_id
  emr_bootstrap_id = dependency.s3.outputs.emr_bootstrap_id
}