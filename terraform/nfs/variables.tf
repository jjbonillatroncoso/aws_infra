variable "subnet_ids" {
  type = list(string)
  description = "vpc subnet ids"
}

variable aws_vpc {
    type = string
    description = "aws vpc"
}