resource "aws_efs_file_system" "open_metadata" {
  creation_token = "open-metadata"

  tags = {
    Name = "Open Metadata"
  }
}



resource "aws_efs_mount_target" "efs" {
  count          = length(var.subnet_ids)
  file_system_id = aws_efs_file_system.open_metadata.id
  subnet_id      = var.subnet_ids[count.index] 
  security_groups = [aws_security_group.eks_efs_access.id]
}

resource "aws_security_group" "eks_efs_access" {
  # ... other configuration ...
  name = "eks-efs-access"
  vpc_id = var.aws_vpc

  egress {
              cidr_blocks      = ["0.0.0.0/0"]
              from_port        = "0"
              protocol         = "-1"
              to_port          = "0"
              description = "terraform generated"
  }

  ingress {
     
              cidr_blocks      = ["10.192.20.0/24","10.192.21.0/24"]
              from_port        = 2049
              protocol         = "tcp"
              to_port          = 2049
              description = "terraform generated"
        
  }
}